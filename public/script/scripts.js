$(function () {
    var loginStatus = false;
    var token = "";
    var apiBaseHeader = "";
    var loginUrl = "/authentication/login/"
    var modal = document.getElementById('modelContainer');
    var API_RESPONSE = [];

    var loginFormVisible = false;
    $(".toggleModel").click(function(){
        /* Show Login form */
        if(!loginFormVisible){
            modal.style.display = 'block';
        } else {
            modal.style.display = "none";
        }
        loginFormVisible = !loginFormVisible;
    });
    
    $(".loginStatus").html("Login Status : " + loginStatus);

    $("form").submit(function(event){
        event.preventDefault();
        apiBaseHeader = $("input[name=baseHeader]").val();
        input = { 
            url: apiBaseHeader + loginUrl,
            method : 'POST',
            inputParam : JSON.stringify({
                username: $("input[name=uname]").val(), 
                password: $("input[name=psw]").val() 
            })
        };
        processRequest(input,'auth');        
    });

    function processRequest(input,action){
        $.ajax({
            url: '/',
            type: 'POST',
            data: input,
            success: function (data) {
                if(action == 'auth'){
                    if(data['token']){
                        loginStatus = true;
                        token = data['token'];
                        $(".loginStatus").html("Login Status : " + loginStatus);
                        modal.style.display = "none";
                        loginFormVisible = false;
                    } else {
                        loginStatus = false;
                    }
                } else {                    
                    if(typeof(data) == 'object'){
                        $(".result").append(
                            '<br> <div class="limitContainerSize">' + 
                                '<div> API - ' +
                                    input['url'] +
                                '</div>' +
                                '<div>' +
                                    JSON.stringify(data, undefined, 2) + 
                                '</div>' + 
                            '</div>'
                        );
                    } else {
                        $(".result").append('<br><div>' + data + '</div>');
                    }
                }
            },
            error : function (err) {
                $(".result").append(
                    '<br <div class="limitContainerSize">' + 
                        '<div class="apiFailed"> API - ' +
                            input['url'] +
                        '</div>' +
                        '<div class="apiFailed">' +
                            err['status'] + "     " + err['statusText'] + "   >>>   " + err['responseText'].substring(0,100) + "..."+
                        '</div>' +
                        '</div>'
                );
            }
        });

    }

	/*
		{
            "url"        : "",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : [],
            "'getParams'"  : {},
            "postParams" : {}
        }
	*/
	
    API_LIST = [
        {
            "url"        : "/authentication/updateLoginDetail/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Node - after login"],
            "getParams"  : {},
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/user/appSettings/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - Angular, app initializer"],
            "getParams"  : {},
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/overviewTilesMeta/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/overviewTilesData/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/serverServiceMeta/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/serverServiceData/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/downtimeHistory/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {    
                "startTime" : 0,
                "endTime"   : 3333333333333,
                "dataLevel" : "server",
                "serverId"  : 1
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/downtimeHistory/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {    
                "startTime" : 0,
                "endTime"   : 3333333333333,
                "dataLevel" : "service",
                "serverId"  : 1
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/downtimeHistory/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {    
                "startTime" : 0,
                "endTime"   : 3333333333333,
                "dataLevel" : "service",
                "serverId"  : 4
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/serverStatus/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {    
                "serviceId" : 1
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/serverStatus/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {    
                "serviceId" : 2
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/getTrends/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {
                "startTime" : 0,
                "endTime" : 3333333333333,
                "dataLevel" : "server",
                "serverId" : 1
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/getTrends/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {
                "startTime" : 0,
                "endTime" : 3333333333333,
                "dataLevel" : "service",
                "assemblyId" : 1
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/getTrends/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {
                "startTime" : 0,
                "endTime" : 3333333333333,
                "dataLevel" : "service",
                "assemblyId" : 4
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/getTrends/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {
                "startTime" : 0,
                "endTime" : 3333333333333,
                "dataLevel" : "service",
                "assemblyId" : 2
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/applicationLogDetail/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {
                "serviceId" : 1
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/applicationLogDetail/",
            "method"     : "GET",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - overview page"],
            "getParams"  : {
                "serviceId" : 1
            },
            "postParams" : {},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/actionTrigger/&ip=0.0.0.1",
            "method"     : "POST",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - Start Services - Invalid request"],
            "getParams"  : {
            },
            "postParams" : {
                "serviceIdList" : '[0]'
			},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/actionTrigger/&ip=0.0.0.1",
            "method"     : "POST",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - Start Services - Invalid request"],
            "getParams"  : {
            },
            "postParams" : {
                "action" : 'start'
			},
            "parmasBasedOnWhichCustomerDb" : ""
        },{
            "url"        : "/toolsMonitoring/actionTrigger/&ip=0.0.0.1",
            "method"     : "POST",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - Start Services - valid request"],
            "getParams"  : {
            },
            "postParams" : {
                "action" : 'start',
                "serviceIdList" : '[0]'
			},
            "parmasBasedOnWhichCustomerDb" : ""
        },
		{
            "url"        : "/toolsMonitoring/actionTrigger/&ip=0.0.0.1",
            "method"     : "POST",
            "exportApi"  : false,
            "appAlias"   : "",
            "usedIn"     : ["Watcher - Start Services - valid request"],
            "getParams"  : {
            },
            "postParams" : {
                "action" : 'start',
                "serviceIdList" : '[1,2,3]'
			},
            "parmasBasedOnWhichCustomerDb" : ""
        }
    ];
    API_RESPONSE = [];
    $(".testAPIs").click(function(event){
        var len = API_LIST.length;
        API_RESPONSE = []
        $(".result").html("")
        for (var index = 0; index < len; index++) {
            const element = API_LIST[index];
            input = { 
                url: apiBaseHeader + element['url'],
                method : element['method'],
                inputParam : '{}',
                header :  token
            };
            if(element['method'] == 'GET'){
                input['inputParam'] = JSON.stringify(element['getParams']);
            } else {
                input['inputParam'] = JSON.stringify(element['postParams']);
            }
            processRequest(input,''); 
        }
    });
});
