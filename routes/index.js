var express = require('express');
var router = express.Router();
var request = require('request');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


/* GET home page. */
router.post('/', function(req, res, next) {
    var options = {
        "url"    : req.body.url,
        "method" : req.body.method,
        "headers" : {
            'Authorization':	"JWT " + req.body.header,
        }
    };
    inputParam = {}
    if(req.body.inputParam){
        inputParam = JSON.parse(req.body.inputParam)
    }    
    if(req.body.method == 'GET'){
        options['qs'] = inputParam;
    } else {
        options['form'] = inputParam;
    }
    request(options).pipe(res);
});


module.exports = router;
